﻿using CapaDatos.ModeloBase;
using EntidadesCompartidas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CapaDatos.Repo
{
    public class RepoUsuario
    {
        public bool AutenticarUsuario(UsuarioDTO usuario)
        {
            UsuarioDTO usu = new UsuarioDTO();
            var retorno = false;

            using (FacturacionBase facturacion = new FacturacionBase())
            {
                try
                {

                    var usuarioBase = facturacion.Usuario.Where(u => u.Usuario1 == usuario.Usuario).FirstOrDefault();

                    if (usuarioBase.Usuario1 == usuario.Usuario && usuarioBase.Contraseña == usuario.Contraseña)
                    {
                        retorno = true;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return retorno;
        }


        public string CrearUsuario(UsuarioDTO usu)
        {
            string resultado = "Se registro al usuario satisfactoriamente.";
            using (FacturacionBase Facturacion = new FacturacionBase())
            {
                
                        Usuario nuevoUsu = new ModeloBase.Usuario();

                        nuevoUsu.Usuario1 = usu.Usuario;
                        nuevoUsu.Contraseña = usu.Contraseña;

                        Facturacion.Usuario.Add(nuevoUsu);
                        Facturacion.SaveChanges();
                    }
                    
            
            return resultado;
        }


        public List<UsuarioDTO> ListarUsuario()
        {
            using (FacturacionBase facturacion = new FacturacionBase())
            {
                var Lista = from u in facturacion.Usuario
                            select new UsuarioDTO
                            {
                                Usuario = u.Usuario1,
                                Contraseña = u.Contraseña,
                            };
                return Lista.ToList();
            }
        }
    }
}
