﻿using CapaDatos.ModeloBase;
using EntidadesCompartidas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos.Repo
{
    public class RepoProducto
    {
        public List<ProductoDTO> ListaProducto()
        {
            using (FacturacionBase facturacion = new FacturacionBase())
            {
                var Lista = from p in facturacion.Producto
                            select new ProductoDTO
                            {
                                ProductoID = p.ProductoID,
                                NombreProducto = p.NombreProducto,
                                Marca = p.Marca,
                                PrecioUnidad = p.PrecioUnidad,
                                Descripcion = p.Descripcion
                            };
                return Lista.ToList();
            }
        }

        public ProductoDTO BuscarProducto(int productoID)
        {
            ProductoDTO produc = new ProductoDTO();
            using (FacturacionBase facturacion = new FacturacionBase())
            {
                var produ = from p in facturacion.Producto
                            select new ProductoDTO
                            {

                                ProductoID = p.ProductoID,
                                NombreProducto = p.NombreProducto,
                                //Marca = p.Marca,
                                //PrecioUnidad = p.PrecioUnidad,
                                Descripcion = p.Descripcion
                            };
                return produ.ToList().FirstOrDefault();
            }
        }



        public string CrearProducto(ProductoDTO produ)
        {
            string resultado = "Se ingreso el Producto";
            Producto nuevoProdu = new Producto();

            nuevoProdu.ProductoID = produ.ProductoID;
            nuevoProdu.NombreProducto = produ.NombreProducto;
            nuevoProdu.Marca = produ.Marca;
            nuevoProdu.PrecioUnidad = produ.PrecioUnidad;
            nuevoProdu.Descripcion = produ.Descripcion;


            using (FacturacionBase Facturacion = new FacturacionBase())
            {
                Facturacion.Producto.Add(nuevoProdu);
                Facturacion.SaveChanges();
            }
            return resultado;
        }








    }
}
