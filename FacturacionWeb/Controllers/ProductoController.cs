﻿using CapaLogica;
using EntidadesCompartidas;
using FacturacionWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FacturacionWeb.Controllers
{
    public class ProductoController : Controller
    {
        // GET: Producto
        LogicaProducto logProdu = new LogicaProducto();


        public ActionResult ListaProducto()
        {
            var lista = logProdu.ListaProducto();
            List<ProductoModel> listaModel = new List<ProductoModel>();

            foreach (ProductoDTO p in lista)
            {
                ProductoModel produ = new ProductoModel();
                produ.ProductoID = p.ProductoID;
                produ.NombreProducto = p.NombreProducto;
                produ.Marca = p.Marca;
                produ.PrecioUnidad = p.PrecioUnidad;
                produ.Descripcion = p.Descripcion;

                listaModel.Add(produ);

            }
            return View(listaModel);
        }



        public ActionResult CrearProducto()
        {
            return View();
        }


        public ActionResult GuardarProducto(ProductoModel model)
        {

            ProductoDTO produ = new ProductoDTO();
            produ.ProductoID = model.ProductoID;
            produ.NombreProducto = model.NombreProducto;
            produ.Marca = model.Marca;
            produ.PrecioUnidad = model.PrecioUnidad;
            produ.Descripcion = model.Descripcion;

            logProdu.CrearProducto(produ);


            return RedirectToAction("ListaProducto");

        }





        public ActionResult DetalleProducto(int id)
        {
            var productoDTOLista = logProdu.ListaProducto();
            var productoDetalle = productoDTOLista.Where(p => p.ProductoID == id).FirstOrDefault();

            ProductoModel modelo = new ProductoModel()
            {
                ProductoID = productoDetalle.ProductoID,
                NombreProducto = productoDetalle.NombreProducto,
                Marca = productoDetalle.Marca,
                PrecioUnidad = productoDetalle.PrecioUnidad,
                Descripcion = productoDetalle.Descripcion,
            };

            return View(modelo);
        }





    }
}