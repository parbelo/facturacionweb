﻿using CapaLogica;
using EntidadesCompartidas;
using FacturacionWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FacturacionWeb.Controllers
{
    public class ClienteController : Controller
    {
        LogicaCliente logCli = new LogicaCliente();

        // GET: Cliente
        public ActionResult ListaCliente()
        {
            var lista = logCli.ListarClientes();
            List<ClienteModel> listaModel = new List<ClienteModel>();

            foreach (ClienteDTO c in lista)
            {
                ClienteModel cli = new ClienteModel();
                cli.ClienteID = c.ClienteID;
                cli.NombreCliente = c.NombreCliente;
                cli.FechaNacimiento = c.FechaNacimiento;
                cli.Domicilio = c.Domicilio;
                cli.Celular = c.Celular;

                listaModel.Add(cli);

            }
            return View(listaModel);
        }




        public ActionResult CrearCliente()
        {
            return View();
        }


        public ActionResult GuardarCliente(ClienteModel model)
        {

            ClienteDTO cli = new ClienteDTO();

            cli.NombreCliente = model.NombreCliente;
            cli.ClienteID = model.ClienteID;
            cli.FechaNacimiento = model.FechaNacimiento;
            cli.Domicilio = model.Domicilio;
            cli.Celular = model.Celular;

            logCli.CrearCliente(cli);

            return RedirectToAction("ListaCliente");

        }



        public ActionResult DetalleCliente(int id)
        {
            var clienteDTOLista = logCli.ListarClientes();
            var clienteDetalle = clienteDTOLista.Where(c => c.ClienteID == id).FirstOrDefault();

            ClienteModel modelo = new ClienteModel()
            {
                ClienteID = clienteDetalle.ClienteID,
                NombreCliente = clienteDetalle.NombreCliente,
                FechaNacimiento = clienteDetalle.FechaNacimiento,
                Domicilio = clienteDetalle.Domicilio,
                Celular = clienteDetalle.Celular,

            };
            return View(modelo);
        }





    }
}