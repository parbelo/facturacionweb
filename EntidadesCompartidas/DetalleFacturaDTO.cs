﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesCompartidas
{
    public class DetalleFacturaDTO
    {

        public int DetalleID { get; set; }

        public decimal Subtotal { get; set; }

        public int FacturaID { get; set; }

        public int ProductoID { get; set; }


    }
}
