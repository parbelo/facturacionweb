﻿using CapaLogica;
using EntidadesCompartidas;
using FacturacionWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FacturacionWeb.Controllers
{
    public class FacturaController : Controller
    {
        //GET: Factura
        private LogicaProducto logicaProducto = new LogicaProducto();
        private LogicaFactura logicaFactura = new LogicaFactura();

        public ActionResult CrearFactura()
        {
            FacturaModel modelSession = new FacturaModel();
            Session.Add("modeloFactura", modelSession);

            ProductoModel prod = new ProductoModel();

            DetalleFacturaModel detalle = new DetalleFacturaModel();

            FacturaModel fac = new FacturaModel();

            fac.detalleFactura.Add(detalle);

            return View();
        }

        [HttpPost]

        public ActionResult CrearFactura(FacturaModel facturaModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    return View(facturaModel);
                }
                ProductoDTO detalleFactura = null;

                DetalleFacturaModel detalleModel = new DetalleFacturaModel();

                ProductoDTO detalleProducto = logicaProducto.BuscarProducto(facturaModel.ProductoID);
                if (detalleFactura != null)
                {
                    ProductoModel productoModel = new ProductoModel()
                    {
                        ProductoID = detalleFactura.ProductoID,
                        NombreProducto = detalleFactura.NombreProducto,
                        PrecioUnidad = facturaModel.MontoTotal,
                    };

                    FacturaModel sessionFacturaModel = new FacturaModel();
                    if (Session["modeloFactura"] != null)
                    {
                        sessionFacturaModel = (FacturaModel)Session["modeloFactura"];
                        sessionFacturaModel.detalleFactura.Add(detalleModel);
                        sessionFacturaModel.ClienteID = facturaModel.ClienteID;
                        sessionFacturaModel.FacturaID = facturaModel.FacturaID;

                        sessionFacturaModel.MontoTotal = CalcularTotal(sessionFacturaModel.detalleFactura);
                        Session["modeloFactura"] = sessionFacturaModel;

                        //return View(sessionFacturaModel);
                    }

                    //sessionFacturaModel.detalleFactura.Add(detalleModel);
                    //sessionFacturaModel.ClienteID = facturaModel.ClienteID;
                    //sessionFacturaModel.FacturaID = facturaModel.FacturaID;

                    //sessionFacturaModel.MontoTotal = CalcularTotal(sessionFacturaModel.detalleFactura);
                    //Session["modeloFactura"] = sessionFacturaModel;

                    return View(sessionFacturaModel);
                }
                else
                {
                    ModelState.AddModelError("ProductoID", $"El producto { facturaModel.ProductoID} no existe");
                    return View(facturaModel);
                }
            }

            catch (Exception)
            {
                ViewBag.mensajeError("Ocurrio un error en el servidor");
                return View("~/Views/Shared/Error.cshtml");
            }

        }
        public decimal CalcularTotal(List<DetalleFacturaModel> detalleFactura)
        {
            decimal precioTotal = 0;
            foreach (DetalleFacturaModel item in detalleFactura)
            {

                item.SubTotal = item.Cantidad * item.Prod.PrecioUnidad;
                precioTotal = item.SubTotal;
            }

            return precioTotal;

        }

        [HttpPost]

        public ActionResult GuardarFactura(FacturaModel facturaModel)
        {
            List<DetalleFacturaDTO> listaProductosDTO = new List<DetalleFacturaDTO>();

            LogicaFactura logFac = new LogicaFactura();
            foreach (var item in facturaModel.detalleFactura)
            {
                DetalleFacturaDTO productoDTO = new DetalleFacturaDTO()
                {
                    ProductoID = item.ProductoID,
                    Subtotal = item.SubTotal,
                    FacturaID = facturaModel.FacturaID,
                };

                listaProductosDTO.Add(productoDTO);
            }

            FacturaDTO factura = new FacturaDTO()
            {
                ClienteID = facturaModel.ClienteID,
                FacturaID = facturaModel.FacturaID,
                CostoTotal = facturaModel.CostoTotal,

                ListaProducto = listaProductosDTO
            };

            ProductoModel prod = new ProductoModel();

            DetalleFacturaModel detalle = new DetalleFacturaModel();

            FacturaModel fac = new FacturaModel();

            fac.detalleFactura.Add(detalle);



            logFac.GuardarFactura(factura);
            return RedirectToAction("Guardar", "_ListaDetalle");
        }





    }
}