﻿using CapaDatos.Repo;
using EntidadesCompartidas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogica
{
    public class LogicaUsuario
    {
        RepoUsuario repoUsu = new RepoUsuario();


        public bool AutenticarUsuario(UsuarioDTO usu)
        {
            return repoUsu.AutenticarUsuario(usu);
        }


        public string CrearUsuario(UsuarioDTO usuario)
        {
            return repoUsu.CrearUsuario(usuario);
        }


        public List<UsuarioDTO> ListarUsuario()
        {
            return repoUsu.ListarUsuario();
        }
    }
}
