﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace FacturacionWeb.Models
{
    public class ClienteModel
    {
        [Required(ErrorMessage = "La cedula o rut del cliente es obligatorio")]
        public int ClienteID { get; set; }

        [Required(ErrorMessage = "La nombre del cliente es obligatorio")]
        public string NombreCliente { get; set; }

        [Required(ErrorMessage = "La fecha de nacimiento del cliente es obligatorio")]
        public DateTime FechaNacimiento { get; set; }

        [Required(ErrorMessage = "El domicilio del cliente es obligatorio")]
        public string Domicilio { get; set; }

        [Required(ErrorMessage = "El celular del cliente es obligatorio")]
        public int Celular { get; set; }
    }
}