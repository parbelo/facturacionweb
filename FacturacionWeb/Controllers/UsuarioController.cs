﻿using CapaLogica;
using EntidadesCompartidas;
using FacturacionWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FacturacionWeb.Controllers
{
    public class UsuarioController : Controller
    {
        LogicaUsuario logUsu = new LogicaUsuario();

        // GET: Usuario
        public ActionResult MostrarLogin()
        {
            return View("Login");
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(UsuarioModel infoDeUsuario)
        {
            CapaLogica.LogicaUsuario logUsu = new CapaLogica.LogicaUsuario();
            UsuarioDTO usu = new UsuarioDTO();

            usu.Usuario = infoDeUsuario.Usuario;
            usu.Contraseña = infoDeUsuario.Contraseña;

            if (logUsu.AutenticarUsuario(usu))
            {
                Session["Usuario"] = infoDeUsuario;
                return View("Home");
            }
            else
            {
                return View("Error");
            }
        }

        
        public ActionResult CrearUsuario()
        {
            return View();
        }


        public ActionResult GuardarUsuario(UsuarioModel model)
        {
            UsuarioDTO usu = new UsuarioDTO();

            usu.Usuario = model.Usuario;
            usu.Contraseña = model.Contraseña;

            logUsu.CrearUsuario(usu);

            return RedirectToAction("ListarUsuario");

        }


        public ActionResult ListarUsuario()
        {
            var lista = logUsu.ListarUsuario();
            List<UsuarioModel> listaModel = new List<UsuarioModel>();

            foreach (UsuarioDTO u in lista)
            {
                UsuarioModel usu = new UsuarioModel();
                usu.Usuario = u.Usuario;
                usu.Contraseña = u.Contraseña;

                listaModel.Add(usu);

            }
            return View(listaModel);
        }
    }
}