﻿using CapaDatos.Repo;
using EntidadesCompartidas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogica
{
    public class LogicaProducto
    {
        RepoProducto repoProdu = new RepoProducto();



        public List<ProductoDTO> ListaProducto()
        {
            return repoProdu.ListaProducto();
        }


        public string CrearProducto(ProductoDTO produ)
        {
            return repoProdu.CrearProducto(produ);
        }



        public ProductoDTO BuscarProducto(int productoID)
        {
            return repoProdu.BuscarProducto(productoID);
        }



    }
}
