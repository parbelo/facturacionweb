﻿using CapaDatos.ModeloBase;
using EntidadesCompartidas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos.Repo
{
    public class RepoFactura
    {
        public void GuardarFactura(FacturaDTO factura)
        {
            Factura facturaCliente = new Factura();

            facturaCliente.FacturaID = factura.FacturaID;
            facturaCliente.ClienteID = factura.ClienteID;
            facturaCliente.MontoTotal = factura.MontoTotal;

            using (FacturacionBase facturacion = new FacturacionBase())
            {
                facturacion.Factura.Add(facturaCliente);
                facturacion.SaveChanges();

                foreach (var detalle in factura.ListaProducto)
                {
                    DetalleFactura listaDetalle = new DetalleFactura();
                    listaDetalle.FacturaID = detalle.FacturaID;
                    listaDetalle.ProductoID = detalle.ProductoID;
                    listaDetalle.Subtotal = detalle.Subtotal;


                    facturacion.DetalleFactura.Add(listaDetalle);
                }
                facturacion.SaveChanges();
            }
        }

        }
    }

