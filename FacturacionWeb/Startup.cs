﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FacturacionWeb.Startup))]
namespace FacturacionWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
