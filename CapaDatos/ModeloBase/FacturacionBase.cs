namespace CapaDatos.ModeloBase
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class FacturacionBase : DbContext
    {
        public FacturacionBase()
            : base("name=FacturacionBase")
        {
        }

        public virtual DbSet<Cliente> Cliente { get; set; }
        public virtual DbSet<DetalleFactura> DetalleFactura { get; set; }
        public virtual DbSet<Factura> Factura { get; set; }
        public virtual DbSet<Producto> Producto { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cliente>()
                .Property(e => e.NombreCliente)
                .IsUnicode(false);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.Domicilio)
                .IsUnicode(false);

            modelBuilder.Entity<DetalleFactura>()
                .Property(e => e.Subtotal)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Factura>()
                .Property(e => e.MontoTotal)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Producto>()
                .Property(e => e.NombreProducto)
                .IsUnicode(false);

            modelBuilder.Entity<Producto>()
                .Property(e => e.Marca)
                .IsUnicode(false);

            modelBuilder.Entity<Producto>()
                .Property(e => e.PrecioUnidad)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Producto>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.Tipo_Usuario)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .HasMany(e => e.Cliente)
                .WithOptional(e => e.Usuario1)
                .HasForeignKey(e => e.Usuario);
        }
    }
}
