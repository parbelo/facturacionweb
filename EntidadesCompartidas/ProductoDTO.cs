﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesCompartidas
{
    public class ProductoDTO
    {
        public int ProductoID { get; set; }

        public string NombreProducto { get; set; }

        public string Marca { get; set; }

        public decimal PrecioUnidad { get; set; }

        public string Descripcion { get; set; }

    }
}
