﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesCompartidas
{
    public class FacturaDTO
    {

        public int FacturaID { get; set; }

        public int ProductoID { get; set; }

        public DateTime FechaFactura { get; set; }

        public decimal MontoTotal { get; set; }

        public int? ClienteID { get; set; }

        
        public List<DetalleFacturaDTO> ListaProducto { get; set; } = new List<DetalleFacturaDTO>();

        public decimal CostoTotal { get; set; }

    }
}
