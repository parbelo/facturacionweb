﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace FacturacionWeb.Models
{
    public class DetalleFacturaModel
    {
        [Required(ErrorMessage = "Este campo es obligatorio")]
        public int ProductoID { get; set; }


        public ProductoModel Prod { get; set; }

        [Required(ErrorMessage = "Este campo es obligatorio")]
        public int Cantidad { get; set; }

        [Required(ErrorMessage = "Este campo es obligatorio")]
        public decimal SubTotal { get; set; }
    }
}