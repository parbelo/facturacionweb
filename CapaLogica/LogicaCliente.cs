﻿using CapaDatos.Repo;
using EntidadesCompartidas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogica
{
    public class LogicaCliente
    {

        RepoCliente repoCli = new RepoCliente();


        public List<ClienteDTO> ListarClientes()
        {
            return repoCli.ListaCliente();
        }



        public string CrearCliente(ClienteDTO cliente)
        {
            return repoCli.CrearCliente(cliente);
        }


    }
}
