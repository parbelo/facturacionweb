﻿using CapaDatos.ModeloBase;
using EntidadesCompartidas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos.Repo
{
    public class RepoCliente
    {


        public List<ClienteDTO> ListaCliente()
        {
            using (FacturacionBase facturacion = new FacturacionBase())
            {
                var Lista = from c in facturacion.Cliente
                            select new ClienteDTO
                            {
                                ClienteID = c.ClienteID,
                                NombreCliente = c.NombreCliente,
                                FechaNacimiento = c.FechaNacimiento,
                                Domicilio = c.Domicilio,
                                Celular = c.Celular,

                            };
                return Lista.ToList();
            }
        }



        public string CrearCliente(ClienteDTO cli)
        {
            string resultado = "Se ingreso el Cliente";
            using (FacturacionBase Facturacion = new FacturacionBase())
            {
                Cliente nuevoCli = new Cliente();

                nuevoCli.ClienteID = cli.ClienteID;
                nuevoCli.NombreCliente = cli.NombreCliente;
                nuevoCli.FechaNacimiento = cli.FechaNacimiento;
                nuevoCli.Domicilio = cli.Domicilio;
                nuevoCli.Celular = cli.Celular;

                Facturacion.Cliente.Add(nuevoCli);
                Facturacion.SaveChanges();
            }
            return resultado;
        }
    }
}
