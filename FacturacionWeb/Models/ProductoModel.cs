﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FacturacionWeb.Models
{
    public class ProductoModel
    {
        [Required(ErrorMessage = "El id del producto es obligatorio")]
        public int ProductoID { get; set; }

        [Required(ErrorMessage = "El nombre del producto es obligatorio")]
        public string NombreProducto { get; set; }

        [Required(ErrorMessage = "La marca del producto es obligatorio")]
        public string Marca { get; set; }

        [Required(ErrorMessage = "El precio del producto es obligatorio")]
        public decimal PrecioUnidad { get; set; }

        [Required(ErrorMessage = "Una breve descripcion es obligatoria")]
        public string Descripcion { get; set; }
    }
}