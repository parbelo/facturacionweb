
CREATE DATABASE FacturacionBase
GO
USE FacturacionBase

CREATE TABLE Usuario
(
	Usuario NVARCHAR (20) PRIMARY KEY NOT NULL,
	Contrase�a NVARCHAR (50) NOT NULL,
	Tipo_Usuario VARCHAR (20) NOT NULL,
)
ALTER TABLE Usuario ADD CONSTRAINT Tipo_Usuario CHECK (TipoUsuario='Usuario' OR TipoUsuario='Administrador')

GO

CREATE TABLE Cliente 
(
	ClienteID INT PRIMARY KEY NOT NULL,
	NombreCliente VARCHAR (50) NOT NULL,
	FechaNacimiento DATE NOT NULL,
	Domicilio VARCHAR (50) NOT NULL,
	Celular INT,
	
	Usuario NVARCHAR (20) FOREIGN KEY REFERENCES Usuario(Usuario)
)

GO

CREATE TABLE Producto
(
	ProductoID INT PRIMARY KEY NOT NULL,
	NombreProducto VARCHAR (50) NOT NULL,
	Marca VARCHAR (50) NOT NULL,
	PrecioUnidad DECIMAL NOT NULL,
	Descripcion VARCHAR (50),
)

GO

CREATE TABLE Factura
(
	FacturaID INT PRIMARY KEY NOT NULL,
	FechaFactura DATE NOT NULL,
	MontoTotal DECIMAL NOT NULL,

	ClienteID INT FOREIGN KEY REFERENCES Cliente(ClienteID)
)
GO

CREATE TABLE DetalleFactura
(
	DetalleID INT PRIMARY KEY IDENTITY (1,1) NOT NULL,
	Subtotal DECIMAL NOT NULL,

	FacturaID INT FOREIGN KEY REFERENCES Factura (FacturaID),
	ProductoID INT FOREIGN KEY REFERENCES Producto (ProductoID)
)
GO

INSERT INTO Usuario VALUES ('SuperAdmin', '5upeR@dmin', 'Administrador')
INSERT INTO Usuario VALUES ('MRODRIGUEZ', '1234', 'Usuario')
INSERT INTO Usuario VALUES ('BMULLER', '5678', 'Usuario')
SELECT *FROM Usuario


INSERT INTO Cliente VALUES (46778590 , 'Administrador General', '10/07/1995', 'Calle 11 Ap 0092 esq Calle 13',0,'SuperAdmin')
INSERT INTO Cliente VALUES (67889471 , 'Marcos Rodriguez', '10/04/1992', 'Calle A 1223 esq Calle E',093774885,'MRODRIGUEZ')
INSERT INTO Cliente VALUES (55788940 , 'Belen Muller', '09/09/1965', 'Calle E 2234 esq Calle Q',099887766,'BMULLER')
SELECT *FROM Cliente


INSERT INTO Producto VALUES (1, 'Iphone' , '8 Plus', 42000, '32gb - Color Blanco')
INSERT INTO Producto VALUES (2, 'Samsung' , 'S8 +', 36500, '64 gb - Color Plat�')
INSERT INTO Producto VALUES (3, 'LG' , 'LK10', 2500, 'Color Azul oscuro')
INSERT INTO Producto VALUES (4, 'Iphone' , '7 Plus', 38000, '32 gb - Color Plata')
SELECT *FROM Producto

INSERT INTO Factura VALUES (001,'01/10/2017', 42000 , 46778590)
INSERT INTO Factura VALUES (003, '01/10/2017',2500, 67889471)
SELECT *FROM Factura

INSERT INTO DetalleFactura VALUES (42000, 1, 1)
INSERT INTO DetalleFactura VALUES (2500,3,3 )
SELECT * FROM DetalleFactura