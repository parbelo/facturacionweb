﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace FacturacionWeb.Models
{
    public class FacturaModel
    {
        [Required(ErrorMessage = "Este campo es obligatorio")]
        public int FacturaID { get; set; }

        [Required(ErrorMessage = "Este campo es obligatorio")]
        public int ProductoID { get; set; }


        public ClienteModel Cliente { get; set; }

        [Required(ErrorMessage = "Este campo es obligatorio")]
        public int Cantidad { get; set; }


        public DateTime FechaFactura { get; set; }

        [Required(ErrorMessage = "Este campo es obligatorio")]
        public decimal MontoTotal { get; set; }

        [Required(ErrorMessage = "Este campo es obligatorio")]
        public int? ClienteID { get; set; }

        public List<DetalleFacturaModel> detalleFactura { get; set; } = new List<DetalleFacturaModel>();

        [Required(ErrorMessage = "Este campo es obligatorio")]
        public decimal CostoTotal { get; set; }

        
    }
}